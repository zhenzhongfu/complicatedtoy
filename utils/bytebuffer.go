package utils

import (
	"math"
	"sync"
)

type ByteBuffer struct {
	B []byte
}

func (bb *ByteBuffer) Len() int {
	return len(bb.B)
}

func (bb *ByteBuffer) Write(b []byte) (int, error) {
	bb.B = append(bb.B, b...)
	return len(b), nil
}

func (bb *ByteBuffer) Resize(len int) {
	if cap(bb.B) < len {
		b := make([]byte, len)
		bb.B = b
	} else {
		bb.B = bb.B[:len]
	}
}

type ByteBufferPool struct {
	defaultLen int
	minLen     int
	maxLen     int
	pool       sync.Pool

	bufStat []int
	mutex   sync.Mutex
}

const (
	MIN_LEN          = 256
	MAX_LEN          = 65535
	DEFAULT_LEN      = 1024
	ADJUST_THRESHOLD = 1000000
)

var (
	MIN_BIT   = int(math.Ceil(math.Log2(MIN_LEN))) + 1 // 最小分配指数
	BIT_SCOPE = int(math.Ceil(math.Log2(MAX_LEN))) + 1 // 最大分配指数
)

var bytebufferPool = &ByteBufferPool{
	minLen:     MIN_LEN,
	defaultLen: MAX_LEN,
	maxLen:     DEFAULT_LEN,            // 2^8~2^16
	bufStat:    make([]int, BIT_SCOPE), // 2倍作为步长,总共9个slot
}

func GetByteBuffer() *ByteBuffer {
	return bytebufferPool.Get()
}

func PutByteBuffer(bb *ByteBuffer) error {
	return bytebufferPool.Put(bb)
}

func (bp *ByteBufferPool) Get() *ByteBuffer {
	bb := bp.pool.Get()
	if bb != nil {
		bb.(*ByteBuffer).B = bb.(*ByteBuffer).B[:0]
		return bb.(*ByteBuffer)
	}
	return &ByteBuffer{
		B: make([]byte, 0, bp.defaultLen), // 初始化默认大小
	}
}

func (bp *ByteBufferPool) Put(bb *ByteBuffer) error {
	len := bb.Len()
	idx := findStep(len)

	bp.mutex.Lock()
	if bp.bufStat[idx] > ADJUST_THRESHOLD {
		bp.Adjust()
	}
	bp.bufStat[idx] += 1
	bp.mutex.Unlock()

	bp.pool.Put(bb)
	return nil
}

func (bp *ByteBufferPool) Adjust() error {
	maxnum := 0
	maxidx := 0
	for index, count := range bp.bufStat {
		if count > maxnum {
			maxnum = count
			maxidx = index
		}
		bp.bufStat[index] = 0
	}
	bp.defaultLen = int(math.Pow(2, float64(maxidx)))

	return nil
}

// 查找步长
func findStep(len int) int {
	bit := int(MIN_BIT)
	for {
		value := 1 << (uint)(bit)
		if value > len {
			return bit // 1<<(bit+1) = 2^bit
		}
		bit += 1
	}
}

package utils

import "sync"

type SyncMap struct {
	m sync.Map
}

func NewSyncMap() *SyncMap {
	return &SyncMap{}
}

func (mm *SyncMap) Insert(k, v interface{}) {
	mm.m.Store(k, v)
}

func (mm *SyncMap) Remove(k interface{}) {
	mm.m.Delete(k)
}

func (mm *SyncMap) GetNum() int {
	num := 0
	mm.m.Range(func(key, value interface{}) bool {
		num += 1
		return true
	})
	return num
}

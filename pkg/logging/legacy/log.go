package logging

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"time"
)

const (
	modeLine   int = iota
	modeFormat     = 1
)

type Loglvl int

const (
	DEBUG Loglvl = iota
	INFO
	WARN
	ERROR
	FATAL
)

const (
	gapMinute = time.Minute
	gapHour   = time.Hour
	gapDay    = gapHour * 24
)

var (
	LoggerInstance *Logger
	tagName        = map[Loglvl]string{
		DEBUG: "DEBUG",
		INFO:  "INFO",
		WARN:  "WARN",
		ERROR: "ERROR",
		FATAL: "FATAL",
	}
)

type Logger struct {
	filepath string
	gaptime  time.Duration

	logger     *log.Logger
	lvl        Loglvl
	alsoStdout bool
	writer     *logWriter
}

type logWriter struct {
	file     *os.File
	filepath string
	gaptime  time.Duration
	rotateCh <-chan time.Time
}

func newLogWriter(filepath string, gaptime time.Duration) *logWriter {
	err := os.MkdirAll(filepath, os.ModePerm)
	if filepath != "" {
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return nil
		}
	}
	now := time.Now()
	filename := getFileName(now)
	file, err := os.OpenFile(path.Join(filepath, filename), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		if os.IsNotExist(err) {
			file, err = os.Create(path.Join(filepath, filename))
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				return nil
			}
		} else {
			fmt.Fprintln(os.Stderr, err)
			return nil
		}
	}

	next := now.Truncate(time.Second).Add(gaptime)
	var rotateCh <-chan time.Time
	if gaptime == gapMinute || gaptime == gapHour || gaptime == gapDay {
		rotateCh = time.After(next.Sub(time.Now()))
	}

	return &logWriter{
		file:     file,
		filepath: filepath,
		gaptime:  gaptime,
		rotateCh: rotateCh,
	}
}

func (writer *logWriter) Write(b []byte) (int, error) {
	var err error
	if writer.rotateCh != nil && writer.file != os.Stdout && writer.file != os.Stderr {
		select {
		case now := <-writer.rotateCh:
			writer.file.Close()
			writer.file = nil
			name := getFileName(now)
			writer.file, err = os.Create(path.Join(writer.filepath, name))
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				writer.file = os.Stderr
			} else {
				next := now.Truncate(time.Second).Add(writer.gaptime)
				writer.rotateCh = time.After(next.Sub(now))
			}
		default:
			{
			}
		}
	}
	return writer.file.Write(b)
}

func (writer *logWriter) Close() {
	writer.file.Close()
}

func Start(decors ...func(*Logger) *Logger) *Logger {
	l := &Logger{}
	for _, decor := range decors {
		l = decor(l)
	}

	var writer *logWriter
	if l.filepath != "" {
		writer = newLogWriter(l.filepath, l.gaptime)
		l.logger = log.New(writer, "", log.LstdFlags)
	} else {
		l.logger = log.New(os.Stderr, "", log.LstdFlags)
	}
	return l
}

func (l *Logger) printf(lvl Loglvl, format string, v ...interface{}) {
	if lvl >= l.lvl {
		funcName, fileName, lineName := getRuntimeInfo()
		format = fmt.Sprintf("%5s [%s](%s:%d) - %s", tagName[lvl], path.Base(funcName), path.Base(fileName), lineName, format)
		if l.alsoStdout {
			format2 := fmt.Sprintf("%s %s %s", colorStart(lvl), format, colorEnd())
			log.Printf(format2, v...)
		}
		l.logger.Printf(format, v...)
		if lvl == FATAL {
			os.Exit(1)
		}
	}
}

func (l *Logger) println(lvl Loglvl, v ...interface{}) {
	if lvl >= l.lvl {
		funcName, fileName, lineNum := getRuntimeInfo()
		prefix := fmt.Sprintf("%5s [%s] (%s:%d) - ", tagName[lvl], path.Base(funcName), path.Base(fileName), lineNum)
		value := fmt.Sprintf("%s%s", prefix, fmt.Sprint(v...))
		if l.alsoStdout {
			value2 := fmt.Sprintf("%s %s %s", colorStart(lvl), value, colorEnd())
			log.Print(value2)
		}
		l.logger.Print(value)
		if lvl == FATAL {
			os.Exit(1)
		}
	}
}

func getRuntimeInfo() (string, string, int) {
	pc, fn, ln, ok := runtime.Caller(3) // 3 steps up the stack frame
	if !ok {
		fn = "???"
		ln = 0
	}
	function := "???"
	caller := runtime.FuncForPC(pc)
	if caller != nil {
		function = caller.Name()
	}
	return function, fn, ln
}

func getFileName(t time.Time) string {
	app := path.Base(os.Args[0])
	year := t.Year()
	month := t.Month()
	day := t.Day()
	hour := t.Hour()
	minute := t.Minute()
	pid := os.Getpid()
	return fmt.Sprintf("app.%04d-%02d-%02d-%02d%-02d.%d.log", app, year, month, day, hour, minute, pid)
}

//----------- for color formatting
const (
	colorBlack int = iota + 30
	colorRed
	colorGreen
	colorYellow
	colorBlue
	colorMagenta
	colorCyan
	colorWhite
)

func colorSeq(color int) string {
	return fmt.Sprintf("\033[%dm", color)
}

func colorSeqBold(color int) string {
	return fmt.Sprintf("\033[%d;1m", color)
}

func colorStart(level Loglvl) string {
	switch level {
	case DEBUG:
		return colorSeq(colorWhite)
	case INFO:
		return colorSeqBold(colorWhite)
	case WARN:
		return colorSeqBold(colorYellow)
	case ERROR:
		return colorSeqBold(colorMagenta)
	case FATAL:
		return colorSeqBold(colorRed)
	default:
		return colorSeq(colorWhite)
	}
}

func colorEnd() string {
	return "\033[0m"
}

//--------- for decors
func DebugLevel(l Logger) Logger {
	l.lvl = DEBUG
	return l
}

func InfoLevel(l Logger) Logger {
	l.lvl = INFO
	return l
}

func WarnLevel(l Logger) Logger {
	l.lvl = WARN
	return l
}

func ErrorLevel(l Logger) Logger {
	l.lvl = ERROR
	return l
}

func FatalLevel(l Logger) Logger {
	l.lvl = FATAL
	return l
}

func FilePath(p string) func(*Logger) *Logger {
	return func(l *Logger) *Logger {
		l.filepath = p
		return l
	}
}

func EveryHour(l *Logger) *Logger {
	l.gaptime = gapHour
	return l
}

func EveryMinute(l *Logger) *Logger {
	l.gaptime = gapMinute
	return l
}

func EveryDay(l *Logger) *Logger {
	l.gaptime = gapDay
	return l
}

func AlsoStdout(l *Logger) *Logger {
	l.alsoStdout = true
	return l
}

//----------- logger
func Debugf(format string, v ...interface{}) {
	LoggerInstance.printf(DEBUG, format, v...)
}

func Infof(format string, v ...interface{}) {
	LoggerInstance.printf(INFO, format, v...)
}

func Warnf(format string, v ...interface{}) {
	LoggerInstance.printf(WARN, format, v...)
}

func Errorf(format string, v ...interface{}) {
	LoggerInstance.printf(ERROR, format, v...)
}

func Fatalf(format string, v ...interface{}) {
	LoggerInstance.printf(FATAL, format, v...)
	os.Exit(1)
}

func Debugln(v ...interface{}) {
	LoggerInstance.println(DEBUG, v...)
}

func Infoln(v ...interface{}) {
	LoggerInstance.println(INFO, v...)
}

func Warnln(v ...interface{}) {
	LoggerInstance.println(WARN, v...)
}

func Errorln(v ...interface{}) {
	LoggerInstance.println(ERROR, v...)
}

func Fatalln(v ...interface{}) {
	LoggerInstance.println(FATAL, v...)
	os.Exit(1)
}

package logging

import (
	"github.com/zhenzhongfu/loger"
)

var (
	LogServer *LogSrv
)

const (
	modeLine int = iota
	modeFormat
)

type LogSrv struct {
	filepath string
	mode     int
	logCh    chan *logmsg
}

func Setup(length int) *LogSrv {
	LogServer = NewLogSrv(length)
	return LogServer
}

func NewLogSrv(length int) *LogSrv {
	return &LogSrv{
		logCh: make(chan *logmsg, length),
	}
}

func (l *LogSrv) Serve(decors ...func(loger.Logger) loger.Logger) {
	go func() {
		//defer loger.Start(loger.LogFilePath(l.filepath), loger.EveryDay, loger.AlsoStdout).Stop()
		defer loger.Start(decors...).Stop()
		for {
			select {
			case m := <-l.logCh:
				l.record(m)
			}
		}
	}()
}

func (l *LogSrv) record(m *logmsg) {
	switch loger.LogLevel(m.level) {
	case loger.DEBUG:
		if m.mode == modeLine {
			loger.Debugln(m.msg...)
		} else {
			loger.Debugf(m.format, m.msg...)
		}
	default:
		loger.Debugln(m.msg)
	}
}

type logmsg struct {
	mode   int // 0, Println; 1,Printf
	level  loger.LogLevel
	format string
	msg    []interface{}
}

func NewLogMsg(mode int, level loger.LogLevel, format string, msg ...interface{}) *logmsg {
	l := &logmsg{
		mode:   mode,
		level:  level,
		format: format,
		msg:    make([]interface{}, 0),
	}
	for _, v := range msg {
		l.msg = append(l.msg, v)
	}
	return l
}

//----------- logserver
func Debugln(v ...interface{}) {
	m := NewLogMsg(modeLine, loger.DEBUG, "", v...)
	LogServer.logCh <- m
}

func Debugf(format string, v ...interface{}) {
	m := NewLogMsg(modeFormat, loger.DEBUG, format, v...)
	LogServer.logCh <- m
}

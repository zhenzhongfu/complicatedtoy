package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"time"
	"toy/network"
)

func testMsg() {
	cmd := network.HeartBeatC2S
	body := []byte("hello world")
	msg := network.GetMessage(cmd, body)
	defer network.PutMessage(msg)
}

func testPool() {
	b := network.GetByteBuffer()
	defer network.PutByteBuffer(b)
}

func on() {
	cmd := network.HeartBeatC2S
	body := []byte("hello world")
	msg := network.GetMessage(cmd, body)
	b := network.GetByteBuffer()

	b, err := network.Pack(msg, b)
	if err != nil {
		fmt.Println(err)
	}
	msg2, err := network.Unpack(b.B)
	if err != nil {
		fmt.Println(err)
	}

	defer func() {
		fmt.Println("yoyo")
		network.PutMessage(msg)
		network.PutByteBuffer(b)
		network.PutMessage(msg2)
	}()
}

func main() {
	go func() {
		log.Println(http.ListenAndServe(":8887", nil))
	}()

	fmt.Println(time.Now())
	for i := 0; i < 100000000; i++ {
		testMsg()
	}
	fmt.Println(time.Now())

	time.Sleep(time.Second * 1000000)
}

package main

import (
	"log"
	"net/http"
	_ "net/http/pprof"
	"toy/pkg/logging"

	"github.com/zhenzhongfu/loger"

	"toy/mod"
	"toy/network"
	"toy/utils/setting"
)

func main() {
	// logserver
	logger := logging.Setup(1000000)
	logger.Serve(loger.LogFilePath("./server.log"), loger.EveryDay)

	setting.Setup("../../conf/app.ini")

	//pprof
	go func() {
		log.Println(http.ListenAndServe(":8887", nil))
	}()

	srv := network.NewNetwork()
	srv.Setup(":8888", 1, 30, 30)
	srv.RegistOnConnect(onConnect)
	srv.RegistOnClosed(onClosed)
	srv.RegistOnTimeout(onTimeout)
	mod.Setup(srv.GetRouter())
	srv.Serve()
}

func onConnect(s *network.Session) error {
	logging.Debugln("on connect")
	return nil
}

func onClosed(s *network.Session) error {
	logging.Debugln("on closed")
	return nil
}

func onTimeout(s *network.Session) error {
	logging.Debugln("on timeout")
	return nil
}

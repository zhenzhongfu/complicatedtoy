package main

import (
	"log"
	"net/http"
	_ "net/http/pprof"
	"time"
	"toy/mod"
	"toy/network"
	"toy/pkg/logging"
	pb "toy/protocol"
	"toy/utils/setting"

	"github.com/zhenzhongfu/loger"
)

func main() {
	go func() {
		log.Println(http.ListenAndServe(":8885", nil))
	}()

	// logserver
	logger := logging.Setup(1000000)
	logger.Serve(loger.LogFilePath("./client.log"), loger.EveryDay)

	setting.Setup("../../conf/app.ini")

	n := network.NewNetwork()
	n.Setup(":8888", 1, 30, 30)
	mod.Setup(n.GetRouter())
	n.RegistOnConnect(onConnect)
	n.RegistOnClosed(onClosed)
	n.RegistOnTimeout(onTimeout)

	for i := 0; i < 1000; i++ {
		n.Connect(time.Second)
	}

	n.WaitGroup()
}

func onConnect(s *network.Session) error {
	logging.Debugln("on connect")
	p := &pb.LoginC2SLogin{
		Accname: "larmi",
	}
	cmd := uint32(pb.ModLoginC2SLogin)
	body := p
	s.Send(cmd, body)

	return nil
}

func onClosed(s *network.Session) error {
	logging.Debugln("on closed")
	return nil
}

func onTimeout(s *network.Session) error {
	logging.Debugln("on timeout")

	p := &pb.LoginC2SLogin{
		Accname: "larmi",
	}
	cmd := uint32(pb.ModLoginC2SLogin)
	body := p

	for i := 0; i < 10; i++ {
		s.Send(cmd, body)
	}

	return nil
}

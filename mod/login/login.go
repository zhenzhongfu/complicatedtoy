package login

import (
	"toy/network"
	"toy/pkg/logging"
	pb "toy/protocol"
)

func OnModLoginC2SLogin(h *network.Session, request interface{}) error {
	//fmt.Println("-------------@@@ --", request.(*pb.LoginC2SLogin))
	logging.Debugln("-------------@@@ --", request.(*pb.LoginC2SLogin))

	//encode
	p := &pb.LoginS2CLogin{
		Code: 18,
		LoginInfo: &pb.PLoginInfo{
			Id:   120,
			Name: "who is this? SpiderMan.",
		},
	}
	cmd := uint32(pb.ModLoginS2CLogin)
	body := p
	return h.Send(cmd, body)
}

func OnModLoginS2CLogin(h *network.Session, request interface{}) error {
	logging.Debugln("### client s2c ###", request)
	//fmt.Println("### client s2c ###", request)
	return nil
}

func Notify(h *network.Session, code uint32) error {
	p := &pb.LoginS2CNotify{
		Code: code,
	}
	cmd := uint32(pb.ModLoginS2CNotify)
	body := p
	return h.Send(cmd, body)
}
